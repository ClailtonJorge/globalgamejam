angular.module('ionicApp', ['ionic'])

 .controller('AppCtrl', function($scope, $ionicLoading) {
    
    var x;
    var y;
    var z;  
    
    if (window.DeviceMotionEvent) {
        
      window.addEventListener('devicemotion', deviceMotionHandler, false);
    } else {
      document.getElementById("dmEvent").innerHTML = "Not supported."
    }
    
    function deviceMotionHandler(eventData) {
      var info, xyz = "[X, Y, Z]";
        //window.navigator.vibrate(200); // vibrate for 200ms


      // Grab the acceleration from the results
      var acceleration = eventData.acceleration;
    
    


      // Grab the acceleration including gravity from the results
      acceleration = eventData.accelerationIncludingGravity;
      x = acceleration.x;
      y = acceleration.y;
      z = acceleration.z;
      x = (Math.round(parseFloat(x)*1)/1);
      y = (Math.round(parseFloat(y)*1)/1);
      z = (Math.round(parseFloat(z)*1)/1);        
      $("#x").text(x);
      $("#y").text(y);
      $("#z").text(z);
      sendMessage(x+":"+y);
      if(y>0){
        $("#esquerda").html("");
        $("#direita").html("DIREITA");
        /*sendMessage("DIREITA");*/
      }
      else if(y<0){
        $("#direita").html("");
        $("#esquerda").html("ESQUERDA");
       /* sendMessage("ESQUERDA");*/
      }
      if(x>0){
        $("#baixo").html("");
        $("#cima").html("CIMA");
        /*sendMessage("CIMA");*/
      }
      else if(x<0){
        $("#cima").html("");
        $("#baixo").html("BAIXO");
        /*sendMessage("BAIXO");*/
      }
      //if(x>0){sendMessage(x);}
      

      // Grab the rotation rate from the results
      var rotation = eventData.rotationRate;
      info = xyz.replace("X", rotation.alpha);
      info = info.replace("Y", rotation.beta);
      info = info.replace("Z", rotation.gamma);

      // // Grab the refresh interval from the results
      info = eventData.interval;
      
    }
    

 });
              
              