import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;

import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.WsOutbound;

public class ConnectionWSMobile extends MessageInbound {


	private final String id;
	private ConnectionWS conexaoJogo;
	private int x,y,z;
	
	
	public ConnectionWSMobile(String id) {
		this.id = id;
	}
	
	
	@Override
	protected void onOpen(WsOutbound outbound) {
		setConexaoJogo( WebSocketServlet.getConexaoBrowser(id));
		System.out.println("Entrou com este id"+ id+ " ConexaoJogo: "+ conexaoJogo);
		if(conexaoJogo  == null) {
			try {
				this.getWsOutbound().writeTextMessage(CharBuffer.wrap("Conecte com o browser com este id: "+ id));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
//		conexaoJogo = WebSocketServlet.getConnections().get(Integer.valueOf(id));
		WebSocketServletMobile.getConexoesmobile().add(this);
		String message = String.format("\"%s\" se conectou.", id);
		System.out.println(message);
//		WebSocketServlet.broadcast(message);
	}

	
	@Override
	protected void onBinaryMessage(ByteBuffer arg0) throws IOException {
		throw new RuntimeException("Metodo n�o aceito");		
	}

	
	@Override
	protected void onTextMessage(CharBuffer msg) throws IOException {
//		String[] message = msg.toString().split(":");
//		x = Integer.valueOf(message[0]);
//		y = Integer.valueOf(message[1]);
//		conexaoJogo.getWsOutbound().writeTextMessage(CharBuffer.wrap(x+":"+y));
		System.out.println(CharBuffer.wrap(msg));
		if (conexaoJogo != null) {
		conexaoJogo.getWsOutbound().writeTextMessage(CharBuffer.wrap(msg));
		}
//		WebSocketServlet.broadcast(message);
	}

	
	public ConnectionWS getConexaoJogo() {
		return conexaoJogo;
	}

	
	public void setConexaoJogo(ConnectionWS conexaoJogo) {
		if (conexaoJogo != null) {
			conexaoJogo.setEstaPareado(true);
			conexaoJogo.setConexaoJogo(this);
		}
		this.conexaoJogo = conexaoJogo;
	}


	public String getId() {
		return id;
	}
	
	
}
