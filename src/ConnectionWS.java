import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;

import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.WsOutbound;

public class ConnectionWS extends MessageInbound {
	
	private final String id;
	private ConnectionWSMobile conexaoJogo;
	private boolean estaPareado = false;
	
	
	public ConnectionWS(String id) {
		this.id = id;
	}
	
	@Override
	protected void onOpen(WsOutbound outbound) {
		WebSocketServlet.getConnections().add(this);
		conexaoJogo = WebSocketServletMobile.getConexaoBrowser(id);
		String message = String.format("\"%s\" se conectou.", id);
		System.out.println("Entrou com o browser id: "+ id);
		if (!this.estaPareado) {
			try {
				this.getWsOutbound().writeTextMessage(CharBuffer.wrap("Conecte com o mobile com este id: "+ id));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			};
		}
//		WebSocketServlet.broadcast(message);
	}

	@Override
	protected void onBinaryMessage(ByteBuffer arg0) throws IOException {
		throw new RuntimeException("Metodo n�o aceito");		
	}

	@Override
	protected void onTextMessage(CharBuffer msg) throws IOException {
//		String message = String.format("\"%s\": %s", username, msg.toString());
//		WebSocketServlet.broadcast(message);
		if (conexaoJogo != null) {
			conexaoJogo.getWsOutbound().writeTextMessage(CharBuffer.wrap(msg));
		}
	}

	public ConnectionWSMobile getConexaoJogo() {
		return conexaoJogo;
	}

	public void setConexaoJogo(ConnectionWSMobile conexaoJogo) {
		this.conexaoJogo = conexaoJogo;
	}

	public String getId() {
		return id;
	}

	public boolean isEstaPareado() {
		return estaPareado;
	}

	public void setEstaPareado(boolean estaPareado) {
		this.estaPareado = estaPareado;
	}
	

}
