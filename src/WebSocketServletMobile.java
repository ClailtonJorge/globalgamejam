import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.websocket.StreamInbound;

@WebServlet("/webservletmobile")
public class WebSocketServletMobile  extends org.apache.catalina.websocket.WebSocketServlet {
	
	private static final List<ConnectionWSMobile> conexoesMobile = new ArrayList<>();

	
	@Override
	protected StreamInbound createWebSocketInbound(String subProtocol, HttpServletRequest request) {
		int id = Integer.valueOf(request.getParameter("id"));
		System.out.println("Chegou aqui: "+ id);
		ConnectionWSMobile conexao = new ConnectionWSMobile(String.valueOf(id));
		return conexao;
	}

	
	public static List<ConnectionWSMobile> getConexoesmobile() {
		return conexoesMobile;
	}
	
	
	public static ConnectionWSMobile getConexaoBrowser(String id) {
		for(ConnectionWSMobile item : conexoesMobile) {
			if (item.getId().equals(id)) {
				return item;
			}
		}
		return null;
	}
	
}
