import java.io.IOException;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.websocket.StreamInbound;

@WebServlet("/websocket")
public class WebSocketServlet extends org.apache.catalina.websocket.WebSocketServlet {
	
	private static final List<ConnectionWS> connection = new ArrayList<>();
	private static final List<Integer> listaId = new ArrayList<>();
	private static int CONST_ID = 0;

	@Override
	protected StreamInbound createWebSocketInbound(String subProtocol, HttpServletRequest request) {
		String id;
		ConnectionWS conexao = null;
		try {
			
			id = request.getParameter("id");
			System.out.println("Chegou ate aqui");
			for(ConnectionWS item : connection) {
				if (item.getId().equals(id)) {
					conexao =  item;
				}
			}
			conexao = new ConnectionWS(id);
			setConexaoNoMobile(conexao);
			
		} catch (NullPointerException e) {
			e.printStackTrace();
			conexao = new ConnectionWS(String.valueOf(WebSocketServlet.getID()));
		}

		connection.add(conexao);
		return conexao;
	}

	public static List<ConnectionWS> getConnections() {
		return connection;
	}
	
	public static ConnectionWS getConexaoBrowser(String id) {
		for(ConnectionWS item : connection) {
			if (item.getId().equals(id)) {
				return item;
			}
		}
		return null;
	}

	public static final void broadcast(String message){
		for (ConnectionWS con : WebSocketServlet.getConnections()) {
			try {
				con.getWsOutbound().writeTextMessage( CharBuffer.wrap(message) );
				System.out.println("Enviando mensagem de texto (" + message + ")"); 
				} catch (IOException ioe) {
					System.out.println("Aconteceu um erro"); 
				} 
		} 
	}

	public static int getID() {
		CONST_ID += 1;
		return CONST_ID - 1;
	}
	
	
	private void setConexaoNoMobile(ConnectionWS conexao) {
		ConnectionWSMobile conexaoMobile = WebSocketServletMobile.getConexaoBrowser(conexao.getId());
		if (conexaoMobile != null) {conexaoMobile.setConexaoJogo(conexao);}
	}

}
